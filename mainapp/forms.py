from django.forms import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget

from .models import *

class CreateUserForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username', 'email', 'password1', 'password2']

class UpdateUserForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username', 'email', 'password1', 'password2']
        
class SomeForm(forms.Form):
    foo = forms.CharField(widget=SummernoteWidget())

#################Agenda#####################
class FormAgenda(ModelForm):
	class Meta:
		model = Agenda
		fields = '__all__'

class FormEditAgenda(ModelForm):
	class Meta:
		model = Agenda
		fields = '__all__'
#################SPJ########################
class FormSPJ(ModelForm):
	class Meta:
		model = SPJ
		fields = '__all__'

class FormEditSPJ(ModelForm):
	class Meta:
		model = SPJ
		fields = '__all__'

#################Surat########################
class FormSurat(ModelForm):
	class Meta:
		model = Surat
		fields = '__all__'

class FormEditSurat(ModelForm):
	class Meta:
		model = Surat
		fields = '__all__'

#################Pengurus########################
class FormPengurus(ModelForm):
	class Meta:
		model = Pengurus
		fields = '__all__'

class FormEditPengurus(ModelForm):
	class Meta:
		model = Pengurus
		fields = '__all__'