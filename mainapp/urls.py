from django.urls import path
from django.contrib.auth import views as auth_views
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    ############################################################
    path('dashboard/', views.index, name="index"),
    path('', views.home, name='home'),
    ############################################################

    path('updateuser/', views.updateUserPage, name="updateuser"),
    path('login/', views.loginPage, name="login"),
    path('register/', views.registerPage, name="register"),
    path('logout/', views.logoutUser, name="logout"),
    ############################################################
    # SPJ
    path('SPJ/', views.PageSPJ, name="PageSPJ"),
    path('SPJ/<str:pk>/delete', views.deleteSPJ, name="delSPJ"),
    path('SPJ/update/<str:pk>/', views.updateSPJ, name="upSPJ"),
    path('ajax/loadSPJ/', views.load_SPJ, name="loadSPJ"),
    path('ajax/loadSPJ/edit', views.load_SPJ_edit, name="updateSPJ"),
    ############################################################
    # Agenda
    path('Agenda/', views.PageAgenda, name="PageAgenda"),
    path('Agenda/<str:pk>/delete', views.deleteAgenda, name="delAgenda"),
    path('Agenda/update/<str:pk>/', views.updateAgenda, name="upAgenda"),
    path('ajax/loadAgenda/', views.load_Agenda, name="loadAgenda"),
    path('ajax/loadAgenda/edit', views.load_Agenda_edit, name="updateAgenda"),
    ############################################################
    # Surat
    path('Surat/', views.PageSurat, name="PageSurat"),
    path('Surat/<str:pk>/delete', views.deleteSurat, name="delSurat"),
    path('Surat/update/<str:pk>/', views.updateSurat, name="upSurat"),
    path('ajax/loadSurat/', views.load_Surat, name="loadSurat"),
    path('ajax/loadSurat/edit', views.load_Surat_edit, name="updateSurat"),
    ############################################################
    # Kepengurusan
    path('Pengurus/', views.PagePengurus, name="PagePengurus"),
    path('Pengurus/<str:pk>/delete', views.deletePengurus, name="delPengurus"),
    path('Pengurus/update/<str:pk>/', views.updatePengurus, name="upPengurus"),
    path('ajax/loadPengurus/', views.load_Pengurus, name="loadPengurus"),
    path('ajax/loadPengurus/edit', views.load_Pengurus_edit, name="updatePengurus"),
    ############################################################

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
