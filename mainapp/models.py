from django.db import models
from django.db import connection

# Create your models here.
class Pengurus(models.Model):
    nama = models.CharField(max_length=200)
    nim = models.CharField(max_length=200)
    kelas = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nama
    

class Surat(models.Model):
    STATUS = (('Keluar', 'Keluar'), ('Masuk', 'Masuk'))
    nama = models.CharField(max_length=200)
    foto = models.ImageField(upload_to='images/')
    status = models.CharField(max_length=200, null=True, choices=STATUS)
    deskripsi = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nama

class Agenda(models.Model):
    nama = models.CharField(max_length=200,unique=True)
    foto = models.ImageField(upload_to='images/')
    updated_on = models.DateTimeField(auto_now=True)
    content = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.nama

class SPJ(models.Model):
    nama = models.CharField(max_length=200,unique=True)
    foto = models.ImageField(upload_to='images/', null=True)
    agenda = models.ForeignKey(Agenda, null=True, on_delete=models.SET_NULL)
    pengeluaran = models.IntegerField(null=True, blank=True, default=0)
    pemasukkan = models.IntegerField(null=True, blank=True, default=0)
    updated_on = models.DateTimeField(auto_now=True)
    content = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nama

