from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login, logout
from django.db.models import Sum, Count, Q
from datetime import date
from django.contrib import messages

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from .decorators import unauthenticated_user, allowed_users
from .models import *
from .forms import *



def home(request):
	dAgenda = Agenda.objects.all()
	context = {'dAgenda':dAgenda}
	return render(request,'homeuser/home.html', context)

# Create your views here.
@login_required(login_url='login')
def index(request):
	x = date.today()
	dSurat = Surat.objects.values('id').filter(created_at__year=x.year, created_at__month=x.month, created_at__day=x.day).order_by('id').annotate(Count('id'))
	dSPJ = SPJ.objects.values('nama').filter(created_on__year=x.year, created_on__month=x.month, created_on__day=x.day).order_by('nama').annotate(Count('nama'))
	dPengurus =Pengurus.objects.all().annotate(Count('id'))
	dAgenda =Agenda.objects.values('id').filter(created_on__year=x.year, created_on__month=x.month, created_on__day=x.day).order_by('id').annotate(Count('id'))
	context = {'dSurat':dSurat,'dSPJ':dSPJ,'dPengurus':dPengurus,'dAgenda':dAgenda,'act':'Dashboard'}
	return render(request,'admin/menu/dashboard.html', context)


###################### SPJ ##############################

@login_required(login_url='login')
def PageSPJ(request):
	dSPJ = SPJ.objects.all()
	form = FormSPJ()
	if request.method == 'POST':
		form = FormSPJ(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('/SPJ/')
	
	context = {'dSPJ': dSPJ, 'act':'SPJ', 'form':form}
	return render(request, 'admin/menu/adminSPJ.html', context)

def deleteSPJ(request, pk):
	dSPJ = SPJ.objects.get(id=pk)
	# form = FormSPj()

	if request.method == 'POST':
		dSPJ.delete()
		return redirect('/SPJ/')

def updateSPJ(request,pk):
	dSPJ = SPJ.objects.get(id=pk)
	formup = FormEditSPJ(instance=dSPJ)
	if request.method == 'POST':
		formup = FormEditSPJ(request.POST, instance=dSPJ)
		if formup.is_valid():
			formup.save()
			return redirect('/SPJ/')

def load_SPJ(request):
	idb = request.GET.get('SPJ')
	dSPJ = SPJ.objects.get(id=idb)
	formup = FormEditSPJ(instance=dSPJ)
	context = {'formup': formup, 'idb':idb}

	return render(request, 'admin/partial/updateSPJ.html', context)

def load_SPJ_edit(request, pk):
	dSPJ = SPJ.objects.get(id=pk)
	formup = FormEditSPJ(instance=dSPJ)
	if request.method == 'POST':
		formup = FormEditSPJ(request.POST, instance=dSPJ)
		if formup.is_valid():
			formup.save()
			return redirect('/SPJ/')

###################### Agenda ##############################
@login_required(login_url='login')
def PageAgenda(request):
	dAgenda = Agenda.objects.all()
	form = FormAgenda()
	if request.method == 'POST':
		form = FormAgenda(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('/Agenda/')
	
	context = {'dAgenda': dAgenda, 'act':'Agenda', 'form':form}
	return render(request, 'admin/menu/adminAgenda.html', context)

def deleteAgenda(request, pk):
	dAgenda = Agenda.objects.get(id=pk)
	# form = FormSPj()

	if request.method == 'POST':
		dAgenda.delete()
		return redirect('/Agenda/')

def updateAgenda(request,pk):
	dAgenda = Agenda.objects.get(id=pk)
	formup = FormEditAgenda(instance=dAgenda)
	if request.method == 'POST':
		formup = FormEditAgenda(request.POST, instance=dAgenda)
		if formup.is_valid():
			formup.save()
			return redirect('/Agenda/')

def load_Agenda(request):
	idb = request.GET.get('Agenda')
	dAgenda = Agenda.objects.get(id=idb)
	formup = FormEditAgenda(instance=dAgenda)
	context = {'formup': formup, 'idb':idb}

	return render(request, 'admin/partial/updateAgenda.html', context)

def load_Agenda_edit(request, pk):
	dAgenda = Agenda.objects.get(id=pk)
	formup = FormEditAgenda(instance=dAgenda)
	if request.method == 'POST':
		formup = FormEditAgenda(request.POST, instance=dAgenda)
		if formup.is_valid():
			formup.save()
			return redirect('/Agenda/')

###################### Surat ##############################
@login_required(login_url='login')
def PageSurat(request):
	dSurat = Surat.objects.all()
	form = FormSurat()
	if request.method == 'POST':
		form = FormSurat(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('/Surat/')
	
	context = {'dSurat': dSurat, 'act':'Surat', 'form':form}
	return render(request, 'admin/menu/adminSurat.html', context)

def deleteSurat(request, pk):
	dSurat = Surat.objects.get(id=pk)
	# form = FormSPj()

	if request.method == 'POST':
		dSurat.delete()
		return redirect('/Surat/')

def updateSurat(request,pk):
	dSurat = Surat.objects.get(id=pk)
	formup = FormEditSurat(instance=dSurat)
	if request.method == 'POST':
		formup = FormEditSurat(request.POST, instance=dSurat)
		if formup.is_valid():
			formup.save()
			return redirect('/Surat/')

def load_Surat(request):
	idb = request.GET.get('Surat')
	dSurat = Surat.objects.get(id=idb)
	formup = FormEditSurat(instance=dSurat)
	context = {'formup': formup, 'idb':idb}

	return render(request, 'admin/partial/updateSurat.html', context)

def load_Surat_edit(request, pk):
	dSurat = Surat.objects.get(id=pk)
	formup = FormEditSurat(instance=dSurat)
	if request.method == 'POST':
		formup = FormEditSurat(request.POST, instance=dSurat)
		if formup.is_valid():
			formup.save()
			return redirect('/Surat/')


###################### Kepengurusan ##############################
@login_required(login_url='login')
def PagePengurus(request):
	dPengurus = Pengurus.objects.all()
	form = FormPengurus()
	if request.method == 'POST':
		form = FormPengurus(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/Pengurus/')
	
	context = {'dPengurus': dPengurus, 'act':'Kepengurusan', 'form':form}
	return render(request, 'admin/menu/adminKepengurusan.html', context)

def deletePengurus(request, pk):
	dPengurus = Pengurus.objects.get(id=pk)
	# form = FormSPj()

	if request.method == 'POST':
		dPengurus.delete()
		return redirect('/Pengurus/')

def updatePengurus(request,pk):
	dPengurus = Pengurus.objects.get(id=pk)
	formup = FormEditPengurus(instance=dPengurus)
	if request.method == 'POST':
		formup = FormEditPengurus(request.POST, instance=dPengurus)
		if formup.is_valid():
			formup.save()
			return redirect('/Pengurus/')

def load_Pengurus(request):
	idb = request.GET.get('Pengurus')
	dPengurus = Pengurus.objects.get(id=idb)
	formup = FormEditPengurus(instance=dPengurus)
	context = {'formup': formup, 'idb':idb}

	return render(request, 'admin/partial/updatePengurus.html', context)

def load_Pengurus_edit(request, pk):
	dPengurus = Pengurus.objects.get(id=pk)
	formup = FormEditPengurus(instance=dPengurus)
	if request.method == 'POST':
		formup = FormEditPengurus(request.POST, instance=dPengurus)
		if formup.is_valid():
			formup.save()
			return redirect('/Pengurus/')

########################## User ################################
@login_required(login_url='login')
def updateUserPage(request):
	user = request.user
	formup = UpdateUserForm(instance=user)
	
	if request.method == 'POST':
		password_1=request.POST['password1']
		password_2=request.POST['password2']
		formup = UpdateUserForm(request.POST, instance=user)    
		if formup.is_valid():
			user.set_password(password_1)
			user.save()
	context = {'formup':formup,'act':'updateuser'} 
	return render(request, 'admin/edituserPage.html',context)

def logoutUser(request):
	logout(request)
	return redirect('login')

@unauthenticated_user
def registerPage(request):

	form = CreateUserForm()
	if request.method == 'POST':
		form = CreateUserForm(request.POST)
		if form.is_valid():
			user = form.save()
			username = form.cleaned_data.get('username')


			messages.success(request, 'Account was created for ' + username)

			return redirect('login')
		

	context = {'form':form}
	return render(request, 'admin/registerPage.html', context)

@unauthenticated_user
def loginPage(request):

	if request.method == 'POST':
		username = request.POST.get('username')
		password =request.POST.get('password')

		user = authenticate(request, username=username, password=password)

		if user is not None:
			login(request, user)
			return redirect('index')
		else:
			messages.info(request, 'Username OR password is incorrect')

	context = {}
	return render(request, 'admin/loginPage.html', context)