from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from .models import *

class PostAdmin(SummernoteModelAdmin):
    summernote_fields = ('content',)

admin.site.register(Agenda, PostAdmin)
admin.site.register(Surat)
admin.site.register(Pengurus)
admin.site.register(SPJ)

# Register your models here.
